system.push({
    text: "控制板",
    tooltip: "完成产品各项功能",
    children: [
        { text: "+12V1供电电路", tooltip: "产生+12V1供电电源" },
        { text: "+5V1供电电路", tooltip: "产生+5V1供电电源" },
        { text: "+5V供电电路", tooltip: "产生+5V供电电源" },
        { text: "+8V供电电路", tooltip: "产生+8V供电电源" },
        { text: "氧分压检测基准电路", tooltip: "产生氧分压检测基准电源" },
        { text: "抗欠压浪涌电路", tooltip: "抗欠压浪涌" },
        { text: "进气压力检测电路", tooltip: "进气压力检测" },
        { text: "座舱压力检测电路", tooltip: "座舱压力检测" },
        { text: "氧分压信号放大电路", tooltip: "氧分压信号放大" },
        { text: "逻辑转换电路", tooltip: "氧分压信号逻辑转换" },
        { text: "RS422通信电路", tooltip: "RS422通信" },
        { text: "CAN总线电路", tooltip: "CAN总线通讯" },
        { text: "CPU电路", tooltip: "功能控制" },
        { text: "看门狗电路", tooltip: "防止程序跑飞" },
        { text: "+5VF2供电电路", tooltip: "产生+5VF2供电电源" },
        { text: "工作模式选择电路", tooltip: "工作模式选择" },
        { text: "报警控制电路", tooltip: "报警控制" },
        { text: "电磁阀控制电路", tooltip: "电磁阀控制" },
    ],
    items: [
        {
            text: "电容C80", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "15V DC/DC电源输入对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "15V DC/DC电源输入超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "15V DC/DC电源输入超差" },]
        },
        {
            text: "电容C21", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "15V DC/DC电源输入对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "15V DC/DC电源输入超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "15V DC/DC电源输入超差" },]
        },
        {
            text: "三端稳压器V12", tooltip: "电压转换", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源无输出" },
            { bad_type: "电性能失效", bad_reason: "器件损坏", bad_result: "+12V1电源无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "+12V1电源无输出" },
            { bad_type: "输出错误", bad_reason: "器件损坏", bad_result: "+12V1电源输出错误" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "+12V1电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "+12V1电源无输出" },]
        },
        {
            text: "电容C81", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C78", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C20", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C15", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C103", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C33", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C43", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "电容C45", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+12V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+12V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+12V1电源输出超差" },]
        },
        {
            text: "三端稳压器V8", tooltip: "电压转换", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源无输出" },
            { bad_type: "电性能失效", bad_reason: "器件损坏", bad_result: "+5V1电源无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "+5V1电源无输出" },
            { bad_type: "输出错误", bad_reason: "器件损坏", bad_result: "+5V1电源输出错误" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "+5V1电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "+5V1电源无输出" },]
        },
        {
            text: "电容C79", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C19", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C17", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C29", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C89", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C75", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C50", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C76", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C47", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C58", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C59", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C5", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C12", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C39", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C6", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C13", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V1电源输出超差" },]
        },
        {
            text: "电容C87", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "15V DC/DC电源输入对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "15V DC/DC电源输入超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "15V DC/DC电源输入超差" },]
        },
        {
            text: "磁珠L31", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "磁珠L32", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电源调整器N14", tooltip: "电压转换", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "电性能失效", bad_reason: "器件损坏", bad_result: "+5V电源无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "+5V电源无输出" },
            { bad_type: "输出错误", bad_reason: "器件损坏", bad_result: "+5V电源输出错误" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "+5V电源无输出" },]
        },
        {
            text: "电阻R103", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "整流二极管D49", tooltip: "整流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电感L19", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源无输出" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C88", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },]
        },
        {
            text: "电容C91", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源输出超差" },]
        },
        {
            text: "磁珠L20", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "磁珠L30", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C99", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源输出超差" },]
        },
        {
            text: "电容C181", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源输出超差" },]
        },
        {
            text: "电容C250", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源输出超差" },]
        },
        {
            text: "电容C101", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+5V电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+5V电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5V电源输出超差" },]
        },
        {
            text: "电阻R104", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+8V1、+8V2电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "三极管V14", tooltip: "放大", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+8V1、+8V2电源输出错误" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+8V2电源无输出" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "稳压二极管V21", tooltip: "稳压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+8V1、+8V2电源输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+8V1、+8V2电源输出错误" },
            { bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+8V1、+8V2电源无输出" },]
        },
        {
            text: "电容C72", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+8V2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+8V2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+8V2电源输出超差" },]
        },
        {
            text: "电容C73", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+8V1电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+8V1电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+8V1电源输出超差" },]
        },
        {
            text: "电容C27", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "+8V2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "+8V2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+8V2电源输出超差" },]
        },
        {
            text: "电阻R78", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1、12V2基准无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "稳压二极管V4", tooltip: "稳压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1、12V2基准输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1、12V2基准输出错误" },
            { bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "6V1、12V2基准输出错误" },]
        },
        {
            text: "稳压二极管V22", tooltip: "稳压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1、12V2基准输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1、12V2基准输出错误" },
            { bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "6V1、12V2基准输出错误" },]
        },
        {
            text: "电容C92", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "12V2基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "12V2基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "12V2基准输出超差" },]
        },
        {
            text: "电容C93", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "12V2基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "12V2基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "12V2基准输出超差" },]
        },
        {
            text: "电容C24", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "12V2基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "12V2基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "12V2基准输出超差" },]
        },
        {
            text: "电容C82", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "12V2基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "12V2基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "12V2基准输出超差" },]
        },
        {
            text: "电阻R105", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1基准输出错误" },]
        },
        {
            text: "电阻R107", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1基准输出错误" },]
        },
        {
            text: "电容C94", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "6V1基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准输出超差" },]
        },
        {
            text: "电容C83", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "6V1基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准输出超差" },]
        },
        {
            text: "电容C23", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "6V1基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准输出超差" },]
        },
        {
            text: "电容C22", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "15V DC/DC电源输入对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "15V DC/DC电源输入超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "15V DC/DC电源输入超差" },]
        },
        {
            text: "运算放大器N15", tooltip: "电压跟随", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "6V1、12V2基准无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "6V1、12V2基准无输出" },
            { bad_type: "功能失效", bad_reason: "器件老化", bad_result: "6V1、12V2基准无输出" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "6V1、12V2基准无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "6V1、12V2基准输出错误" },
            { bad_type: "4、8脚开路", bad_reason: "断裂，接触不良", bad_result: "6V1、12V2基准无输出" },
            { bad_type: "1、2、3脚开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准无输出" },
            { bad_type: "5、6、7脚开路", bad_reason: "断裂，接触不良", bad_result: "6V1、12V2基准无输出" },]
        },
        {
            text: "电容C44", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "12V2基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "12V2基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "12V2基准输出超差" },]
        },
        {
            text: "电容C86", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "6V1基准对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "6V1基准输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "6V1基准输出超差" },]
        },
        {
            text: "电阻R5", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能异常" },]
        },
        {
            text: "电阻R6", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能异常" },]
        },
        {
            text: "电阻R9", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能异常" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能异常" },]
        },
        {
            text: "电阻R10", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能异常" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能异常" },]
        },
        {
            text: "电容C16", tooltip: "充放电", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },]
        },
        {
            text: "比较器N2", tooltip: "比较", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "电压错误", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "机械故障", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "4、8脚开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "1、2、3脚开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "5、6、7脚开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R204", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "抗欠压浪涌功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R22", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "进气压力信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "磁珠L50", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "进气压力信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D7", tooltip: "钳位", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "进气压力信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D9", tooltip: "钳位", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "进气压力信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C26", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "进气压力信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "进气压力信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "进气压力信号输出超差" },]
        },
        {
            text: "电阻R33", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "进气压力信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "稳压二极管V5", tooltip: "稳压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "进气压力信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "进气压力信号输出超差" },
            { bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "进气压力信号无输出" },]
        },
        {
            text: "AD转换器N3", tooltip: "AD转换", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "进气压力信号无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "进气压力信号无输出" },
            { bad_type: "装配缺陷", bad_reason: "断裂，接触不良", bad_result: "进气压力信号无输出" },
            { bad_type: "漏电", bad_reason: "器件老化", bad_result: "进气压力信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "进气压力信号输出超差" },
            { bad_type: "叠加失效", bad_reason: "器件老化", bad_result: "进气压力信号无输出" },]
        },
        { text: "J599-15航空插座F脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "进气压力信号无输出" },] },
        { text: "J599-15航空插座G脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "进气压力信号无输出" },] },
        { text: "J599-15航空插座K脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "进气压力信号无输出" },] },
        { text: "控制板PVCC线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板进气压力检测功能失效" },] },
        { text: "控制板PX线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板进气压力检测功能失效" },] },
        { text: "控制板PGND线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板进气压力检测功能失效" },] },
        {
            text: "电阻R109", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R110", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D10", tooltip: "单向导通", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "座舱压力传感器电源输出超差" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器电源输出超差" },]
        },
        {
            text: "二极管D11", tooltip: "单向导通", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "座舱压力传感器电源输出超差" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器电源输出超差" },]
        },
        {
            text: "二极管D12", tooltip: "单向导通", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "座舱压力传感器电源输出超差" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器电源输出超差" },]
        },
        {
            text: "二极管D13", tooltip: "单向导通", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "座舱压力传感器电源输出超差" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器电源输出超差" },]
        },
        {
            text: "运算放大器N7", tooltip: "信号放大", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "功能失效", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "座舱压力传感器信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号无输出" },]
        },
        {
            text: "电阻RP4", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器信号输出超差" },]
        },
        {
            text: "电阻R34", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D15", tooltip: "钳位", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "座舱压力传感器信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D14", tooltip: "钳位", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "座舱压力传感器信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C28", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "座舱压力传感器信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号输出超差" },]
        },
        {
            text: "电阻R23", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "稳压二极管V6", tooltip: "稳压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "座舱压力传感器信号输出超差" },
            { bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "座舱压力传感器信号无输出" },]
        },
        {
            text: "AD转换器N4", tooltip: "AD转换", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "装配缺陷", bad_reason: "断裂，接触不良", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "漏电", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "座舱压力传感器信号输出超差" },
            { bad_type: "叠加失效", bad_reason: "器件老化", bad_result: "座舱压力传感器信号无输出" },]
        },
        {
            text: "电阻R121", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R71", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R122", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R215", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R85", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C445", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R119", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R120", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R150", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R37", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "运算放大器N17", tooltip: "信号放大", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "氧分压放大信号无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "氧分压放大信号无输出" },
            { bad_type: "功能失效", bad_reason: "器件老化", bad_result: "氧分压放大信号无输出" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },]
        },
        {
            text: "电容C203", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R87", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R151", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R86", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R38", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C31", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C84", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R39", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C32", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电容C34", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电容C35", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电容C36", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压放大信号超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号超差" },]
        },
        {
            text: "电阻R40", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R41", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R42", tooltip: "限流", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压放大信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R46", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "比较器N19", tooltip: "比较", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "电压错误", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "机械故障", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },]
        },
        {
            text: "电容C42", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R88", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R89", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "比较器N21", tooltip: "比较", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "电压错误", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "机械故障", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },]
        },
        {
            text: "电容C41", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R90", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R91", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R160", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R92", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C52", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "二极管D17", tooltip: "单向导电", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出错误" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "氧分压逻辑信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "门电路N20", tooltip: "逻辑变换", bad: [{ bad_type: "退化", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "漏电", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "晶体管失效", bad_reason: "器件老化", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },]
        },
        {
            text: "二极管D16", tooltip: "单向导电", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出错误" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "氧分压逻辑信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D18", tooltip: "单向导电", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出错误" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "氧分压逻辑信号输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C46", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电容C56", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R159", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R161", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R210", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R236", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R49", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R238", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R50", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R239", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R51", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R240", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R241", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R242", tooltip: "分压", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电容C85", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电容C53", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电容C54", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电容C55", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电容C57", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压逻辑信号无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "氧分压逻辑信号输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压逻辑信号输出超差" },]
        },
        {
            text: "电阻R154", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R155", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C98", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C95", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "RS422通讯电路N6", tooltip: "RS422通讯", bad: [{ bad_type: "数据错位", bad_reason: "器件老化", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "短路", bad_reason: "器件老化", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "1、3、9、10脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "2、8脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "4脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "5脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "6脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "7脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "11、14、16、20脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "12脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "13、15脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "17、18脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "19脚开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送和接收故障" },]
        },
        {
            text: "电容C100", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C182", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C102", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C199", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C200", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C202", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C201", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C444", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C211", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C212", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R35", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R43", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R44", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R45", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R113", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R118", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R116", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R117", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "TVS管D50", tooltip: "保护", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "RS422通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "TVS管D51", tooltip: "保护", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "RS422通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        { text: "J599-17航空插座G脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "RS422通讯电路发送故障" },] },
        { text: "J599-17航空插座H脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "RS422通讯电路发送故障" },] },
        { text: "J599-17航空插座J脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "RS422通讯电路接收故障" },] },
        { text: "J599-17航空插座K脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "RS422通讯电路接收故障" },] },
        { text: "J599-17航空插座L脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "无影响" },] },
        { text: "控制板Y2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板RS422通讯发送故障" },] },
        { text: "控制板Z2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板RS422通讯发送故障" },] },
        { text: "控制板A2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板RS422通讯接收故障" },] },
        { text: "控制板B2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板RS422通讯接收故障" },] },
        { text: "控制板GNDF1线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },] },
        {
            text: "电容C96", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CAN通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C97", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CAN通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R152", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R153", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "CAN通讯电路N18", tooltip: "CAN通讯", bad: [{ bad_type: "数据错位", bad_reason: "器件老化", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "短路", bad_reason: "器件老化", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "1、4脚开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "2脚开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路接收故障" },
            { bad_type: "3脚开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送故障" },
            { bad_type: "5、8脚开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "6、7脚开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },]
        },
        {
            text: "电容C206", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C207", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R156", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R157", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R114", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R115", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C213", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "TVS管D52", tooltip: "保护", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "磁珠L16", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "磁珠L17", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CAN通讯电路发送和接收故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        { text: "J599-17航空插座C脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "无影响" },] },
        { text: "J599-17航空插座S脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "CAN通讯电路发送和接收故障" },] },
        { text: "J599-17航空插座T脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "CAN通讯电路发送和接收故障" },] },
        { text: "控制板GNDF4线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },] },
        { text: "控制板CANH2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板CAN通讯发送和接收故障" },] },
        { text: "控制板CANL2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板CAN通讯发送和接收故障" },] },
        {
            text: "磁珠L15", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "CPU电路U1", tooltip: "信号处理", bad: [{ bad_type: "输出退化", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "封装失效", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },
            { bad_type: "过电应力", bad_reason: "电压过大", bad_result: "CPU电路工作失效" },
            { bad_type: "偶然失效", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "CPU电路工作失效" },
            { bad_type: "2、3、4、5、6、7脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },
            { bad_type: "1、8、14、25、26、27、31、35、36脚开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "9、45脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路RS422信号接收功能失效" },
            { bad_type: "10、46脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路RS422信号发送功能失效" },
            { bad_type: "11、12脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "13脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路复位功能故障" },
            { bad_type: "15脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路升级引导控制功能失效" },
            { bad_type: "16脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作周期选择控制功能失效" },
            { bad_type: "17脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路电磁阀过流保护功能失效" },
            { bad_type: "18、19、20、21脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路数据存储功能失效" },
            { bad_type: "22脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路抗欠压浪涌功能失效" },
            { bad_type: "23脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路自检告警信号无输出" },
            { bad_type: "24脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路氧分压低告警信号无输出" },
            { bad_type: "28脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路电磁阀A控制信号无输出" },
            { bad_type: "29脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路电磁阀B控制信号无输出" },
            { bad_type: "30脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路电磁阀C控制信号无输出" },
            { bad_type: "32、33、34脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路座舱压力检测信号无输入" },
            { bad_type: "37、41、42脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路进气压力检测信号无输入" },
            { bad_type: "38、39、40脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路氧分压逻辑信号无输入" },
            { bad_type: "43脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路CAN通讯接收故障" },
            { bad_type: "44脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路CAN通讯发送故障" },
            { bad_type: "47、48脚开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },]
        },
        {
            text: "电容C204", tooltip: "起振", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路工作失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },]
        },
        {
            text: "电容C205", tooltip: "起振", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路工作失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },]
        },
        {
            text: "晶振XT2", tooltip: "起振", bad: [{ bad_type: "起泡/脱皮", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },
            { bad_type: "引线故障", bad_reason: "断裂，接触不良", bad_result: "CPU电路工作失效" },
            { bad_type: "污染", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "电压不当", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "漏电", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },
            { bad_type: "定时失效", bad_reason: "器件老化", bad_result: "CPU电路工作失效" },]
        },
        {
            text: "电阻R162", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C77", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU工作电压对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU工作电压输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU工作电压输出超差" },]
        },
        {
            text: "电容C51", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU工作电压对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU工作电压输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU工作电压输出超差" },]
        },
        {
            text: "电容C37", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路氧分压逻辑信号无输入" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C38", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路氧分压逻辑信号无输入" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "存储器N25", tooltip: "数据存储", bad: [{ bad_type: "数据溢出故障", bad_reason: "器件老化", bad_result: "CPU电路数据存储功能失效" },
            { bad_type: "污染", bad_reason: "器件老化", bad_result: "CPU电路数据存储功能失效" },
            { bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "CPU电路数据存储功能失效" },
            { bad_type: "功能失效", bad_reason: "器件老化", bad_result: "CPU电路数据存储功能失效" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "CPU电路数据存储功能失效" },
            { bad_type: "短路", bad_reason: "器件老化", bad_result: "CPU电路数据存储功能失效" },]
        },
        {
            text: "电容C208", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C209", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路复位功能故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C67", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C210", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R209", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R244", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R237", tooltip: "限流 ", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "CPU电路复位功能故障" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "CPU电路程序无法烧录" },]
        },
        {
            text: "JTAG插座J2", tooltip: "程序烧录", bad: [{ bad_type: "供电引脚(2、3、5、9)开路", bad_reason: "过机械应力、虚焊", bad_result: "CPU电路程序无法烧录" },
            { bad_type: "DDATA引脚(4),CLOCK引脚(7)开路", bad_reason: "过机械应力、虚焊", bad_result: "CPU电路程序无法烧录" },]
        },
        {
            text: "看门狗N26", tooltip: "复位", bad: [{ bad_type: "电性能失效", bad_reason: "器件损坏", bad_result: "看门狗电路复位信号无输出" },
            { bad_type: "功能失效", bad_reason: "器件老化", bad_result: "看门狗电路复位信号无输出" },
            { bad_type: "参数超差", bad_reason: "器件损坏", bad_result: "看门狗电路复位信号无输出" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "看门狗电路复位信号无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "看门狗电路复位信号无输出" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "看门狗电路复位信号无输出" },]
        },
        {
            text: "电阻R24", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C71", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "12V线性电源输入对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "12V线性电源输入超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "12V线性电源输入超差" },]
        },
        {
            text: "三端稳压器V7", tooltip: "电压转换", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5VF2电源无输出" },
            { bad_type: "电性能失效", bad_reason: "器件损坏", bad_result: "+5VF2电源无输出" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "+5VF2电源无输出" },
            { bad_type: "输出错误", bad_reason: "器件损坏", bad_result: "+5VF2电源无输出" },
            { bad_type: "机械失效", bad_reason: "断裂，接触不良", bad_result: "+5VF2电源无输出" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "+5VF2电源无输出" },]
        },
        {
            text: "电容C74", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "二极管V1", tooltip: "单向导电", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: " +5VF2电源输出错误" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C90", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "磁珠L8", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "+5VF2电源无输出" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C3", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "电容C4", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "电容C11", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "电容C40", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "电容C7", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "电容C14", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "电容C402", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: " +5VF2电源对地短路" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: " +5VF2电源输出超差" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: " +5VF2电源输出超差" },]
        },
        {
            text: "磁珠L11", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R14", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R19", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R82", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C90", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R68", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能和工作模式选择控制功能失效" },]
        },
        {
            text: "电阻R102", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能和工作模式选择控制功能失效" },]
        },
        {
            text: "电阻R300", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R72", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R18", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },]
        },
        {
            text: "电阻R20", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },]
        },
        {
            text: "电阻R83", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },]
        },
        {
            text: "电阻R301", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },]
        },
        {
            text: "电阻R74", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },]
        },
        {
            text: "磁珠L12", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C9", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "升级引导控制功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "隔离电路N9", tooltip: "隔离", bad: [{ bad_type: "输出退化", bad_reason: "器件老化", bad_result: "升级引导控制功能失效" },
            { bad_type: "封装失效", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },
            { bad_type: "过电应力", bad_reason: "电压过大", bad_result: "升级引导控制功能失效" },
            { bad_type: "偶然失效", bad_reason: "器件老化", bad_result: "升级引导控制功能失效" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "升级引导控制功能失效" },
            { bad_type: "1、4、5、8脚开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },
            { bad_type: "2、7脚开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "3、6脚开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },]
        },
        {
            text: "隔离电路N11", tooltip: "隔离", bad: [{ bad_type: "输出退化", bad_reason: "器件老化", bad_result: "工作周期选择控制功能和电磁阀过流保护功能失效" },
            { bad_type: "封装失效", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能和电磁阀过流保护功能失效" },
            { bad_type: "过电应力", bad_reason: "电压过大", bad_result: "工作周期选择控制功能和电磁阀过流保护功能失效" },
            { bad_type: "偶然失效", bad_reason: "器件老化", bad_result: "工作周期选择控制功能和电磁阀过流保护功能失效" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "工作周期选择控制功能和电磁阀过流保护功能失效" },
            { bad_type: "1、4、5、8脚开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能和电磁阀过流保护功能失效" },
            { bad_type: "2、7脚开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },
            { bad_type: "3、6脚开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R17", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },]
        },
        {
            text: "电阻R21", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },]
        },
        {
            text: "电阻R84", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },]
        },
        {
            text: "电阻R302", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },]
        },
        {
            text: "电阻R76", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },]
        },
        {
            text: "磁珠L13", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电容C10", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "工作周期选择控制功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "磁珠L14", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "比较器N8", tooltip: "比较", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "电压错误", bad_reason: "器件老化", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "机械故障", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "3、12脚开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },
            { bad_type: "2、4、5、10、11、13脚开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "1、6、7脚开路", bad_reason: "断裂，接触不良", bad_result: "升级引导控制功能失效" },
            { bad_type: "8、9、14脚开路", bad_reason: "断裂，接触不良", bad_result: "工作周期选择控制功能失效" },]
        },
        {
            text: "电阻R200", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        { text: "J599-17航空插座M脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "升级引导控制功能失效" },] },
        { text: "J599-17航空插座W脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "工作周期选择控制功能失效" },] },
        { text: "J599-17航空插座Y脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "工作周期选择控制功能、升级引导控制功能和电磁阀过流保护功能失效" },] },
        { text: "控制板K2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板升级引导控制功能失效" },] },
        { text: "控制板K3线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板工作周期选择控制功能失效" },] },
        { text: "控制板KGND2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板升级引导控制功能和工作周期选择控制功能失效" },] },
        {
            text: "电阻R47", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号无输入" },]
        },
        {
            text: "电阻R48", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号无输入" },]
        },
        {
            text: "电阻R96", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R97", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "隔离电路N13", tooltip: "隔离", bad: [{ bad_type: "输出退化", bad_reason: "器件老化", bad_result: "自检告警和氧分压低告警信号无输入" },
            { bad_type: "封装失效", bad_reason: "断裂，接触不良", bad_result: "自检告警和氧分压低告警信号无输入" },
            { bad_type: "过电应力", bad_reason: "电压过大", bad_result: "自检告警和氧分压低告警信号无输入" },
            { bad_type: "偶然失效", bad_reason: "器件老化", bad_result: "自检告警和氧分压低告警信号无输入" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "自检告警和氧分压低告警信号无输入" },
            { bad_type: "1、4、5、8脚开路", bad_reason: "断裂，接触不良", bad_result: "自检告警和氧分压低告警信号无输入" },
            { bad_type: "2、7脚开路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号无输入" },
            { bad_type: "3、6脚开路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号无输入" },]
        },
        {
            text: "电阻R67", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号无输入" },]
        },
        {
            text: "电阻R66", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号无输入" },]
        },
        {
            text: "电容C69", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "自检告警信号常有效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电容C68", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压低告警信号常无效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R65", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号常无效" },]
        },
        {
            text: "电阻R52", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号常有效" },]
        },
        {
            text: "三极管Q4", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "自检告警信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号常有效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "三极管Q3", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压低告警信号常有效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号常无效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "三极管Q2", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "自检告警信号常有效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号常无效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "三极管Q1", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "氧分压低告警信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号常有效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D45", tooltip: "单向导电", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "自检告警信号常无效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "二极管D44", tooltip: "单向导电", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "氧分压低告警信号常有效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "继电器K10", tooltip: "开关", bad: [{ bad_type: "触点断开", bad_reason: "断裂，接触不良", bad_result: "自检告警信号常无效" },
            { bad_type: "触点粘结", bad_reason: "器件老化", bad_result: "自检告警信号常无效" },
            { bad_type: "线圈短、断路", bad_reason: "断裂，接触不良", bad_result: "自检告警信号常无效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "继电器K9", tooltip: "开关", bad: [{ bad_type: "触点断开", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号常无效" },
            { bad_type: "触点粘结", bad_reason: "器件老化", bad_result: "氧分压低告警信号常有效" },
            { bad_type: "线圈短、断路", bad_reason: "断裂，接触不良", bad_result: "氧分压低告警信号常有效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        { text: "J599-17航空插座E脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "氧分压低告警信号常无效" },] },
        { text: "J599-17航空插座F脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "自检告警信号常无效" },] },
        { text: "J599-17航空插座Z脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "自检告警和氧分压低告警信号常无效" },] },
        { text: "控制板QYBJ2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板氧分压低告警功能失效" },] },
        { text: "控制板GZBJ2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板自检告警功能失效" },] },
        { text: "控制板BJDY2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板自检告警和氧分压低告警功能失效" },] },
        { text: "控制板LOUT1线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板自检告警和氧分压低告警功能失效" },] },
        { text: "控制板GNDR1线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板自检告警和氧分压低告警功能失效" },] },
        {
            text: "电阻R73", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A控制信号无输入" },]
        },
        {
            text: "电阻R75", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀B控制信号无输入" },]
        },
        {
            text: "电阻R77", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号无输入" },]
        },
        {
            text: "隔离电路N10", tooltip: "隔离", bad: [{ bad_type: "输出退化", bad_reason: "器件老化", bad_result: "电磁阀A、B控制信号无输入" },
            { bad_type: "封装失效", bad_reason: "断裂，接触不良", bad_result: "电磁阀A、B控制信号无输入" },
            { bad_type: "过电应力", bad_reason: "电压过大", bad_result: "电磁阀A、B控制信号无输入" },
            { bad_type: "偶然失效", bad_reason: "器件老化", bad_result: "电磁阀A、B控制信号无输入" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "电磁阀A、B控制信号无输入" },
            { bad_type: "1、4、5、8脚开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A、B控制信号无输入" },
            { bad_type: "2、7脚开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A控制信号无输入" },
            { bad_type: "3、6脚开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀B控制信号无输入" },]
        },
        {
            text: "隔离电路N12", tooltip: "隔离", bad: [{ bad_type: "输出退化", bad_reason: "器件老化", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "封装失效", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "过电应力", bad_reason: "电压过大", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "偶然失效", bad_reason: "器件老化", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "短路", bad_reason: "电压过大导致击穿", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "1、4、5、8脚开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "2、7脚开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号无输入" },
            { bad_type: "3、6脚开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R2", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R93", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A控制信号常有效" },]
        },
        {
            text: "电阻R79", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A控制信号常无效" },]
        },
        {
            text: "电阻R100", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "三极管Q11", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀A控制信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A控制信号常有效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "整流二极管D46", tooltip: "整流", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀A控制信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "MOS管V9", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀A控制信号常有效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀A控制信号常无效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R12", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R94", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀B控制信号常有效" },]
        },
        {
            text: "电阻R80", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀B控制信号常无效" },]
        },
        {
            text: "电阻R99", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "三极管Q12", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀B控制信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀B控制信号常有效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "整流二极管D47", tooltip: "整流", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀B控制信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "MOS管V10", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀B控制信号常有效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀B控制信号常无效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R11", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "电阻R95", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号常有效" },]
        },
        {
            text: "电阻R81", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号常无效" },]
        },
        {
            text: "电阻R101", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },]
        },
        {
            text: "三极管Q13", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀C控制信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号常有效" },
            { bad_type: "增益等性能退化", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "整流二极管D48", tooltip: "整流", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀C控制信号常无效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "无影响" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "MOS管V11", tooltip: "开关", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀C控制信号常有效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀C控制信号常无效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },]
        },
        {
            text: "电阻R206", tooltip: "取样", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R207", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R208", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电容C403", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能异常" },]
        },
        {
            text: "电容C401", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能异常" },]
        },
        {
            text: "电容C405", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能异常" },]
        },
        {
            text: "电容C404", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能异常" },]
        },
        {
            text: "电容C406", tooltip: "滤波", bad: [{ bad_type: "短路", bad_reason: "电压过大，导致击穿", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能异常" },]
        },
        {
            text: "电阻R209", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R214", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R211", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R212", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R123", tooltip: "分压", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能异常" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "电阻R213", tooltip: "限流", bad: [{ bad_type: "参数漂移", bad_reason: "器件老化", bad_result: "无影响" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        {
            text: "比较器N32", tooltip: "比较", bad: [{ bad_type: "电性能失效", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "过电应力", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "电压错误", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "机械故障", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "参数超差", bad_reason: "器件老化", bad_result: "电磁阀过流保护功能失效" },
            { bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "电磁阀过流保护功能失效" },]
        },
        { text: "J599-15航空插座A脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "电磁阀A、B、C控制信号常无效" },] },
        { text: "J599-15航空插座B脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "电磁阀A控制信号常无效" },] },
        { text: "J599-15航空插座C脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "电磁阀B控制信号常无效" },] },
        { text: "J599-15航空插座D脚", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "过机械应力、虚焊", bad_result: "电磁阀C控制信号常无效" },] },
        { text: "控制板LOUT2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板电磁阀A、B、C控制功能失效" },] },
        { text: "控制板DCFA2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板电磁阀A控制功能失效" },] },
        { text: "控制板DCFB2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板电磁阀B控制功能失效" },] },
        { text: "控制板DCFC2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板电磁阀C控制功能失效" },] },
        { text: "控制板LOUT3线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板电磁阀A、B、C控制功能失效" },] },
        { text: "控制板GNDR2线", tooltip: "信号连接", bad: [{ bad_type: "开路", bad_reason: "断裂，接触不良", bad_result: "控制板电磁阀A、B、C控制功能失效" },] },

    ]
})
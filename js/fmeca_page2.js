/**
 * 生成功能电路报表的js
 */

function generateHeader(list) { //产生表头,传入表头数组
    /**
     * <tr>
                <th>班级</t>
                <th>姓名</th>
                <th>年龄</th>
                <th>电话</th>
            </tr>
     */
    var str = "";
    str += "<tr>"
    for (var i in list) {
        str += "<th>" + list[i] + "</th>"
    }
    str += "</tr>"
    return str
}
function ci(_text,_span){
    return {
        text:_text,
        span:_span
    }
}
function generateHtml(col,header){//根据传送过来的col和header生成表格
    html=""
    //step1:生成表头
    html+=header
    //step2:生成行,一个tr表示一行，其中td表示行中的每一列，顺序排列
    count=0 //计算所有列
    for(var i in col[0]){
        count+=col[0][i].span
    }
    list=new Array(count) //新建那么多个的list项
    //step3：按列输出
    for(var i in col){
        var currentRow=0
        for(var j in col[i]){
            if(typeof(list[currentRow])=="undefined")
                list[currentRow]=[]
            list[currentRow].push(col[i][j])
            currentRow+=col[i][j].span //更新当前列
        }
    }
    //step4:生成表格
    for(var i in list){
        html+="<tr>"
        for(var j in list[i]){
            //<td rowspan="2">601班</td>
            html+="<td rowspan=\""+list[i][j].span+"\">"
            html+=list[i][j].text
            html+="</td>"
        }
        html+="</tr>"
    }

    return html
}
//*********************************************以上为function定义区 */

function makecol(v){
    if(typeof(v)=="undefined") v=[]
}
function log(v){
    console.log(v)
}
onmessage = function (event) { //前台传递给后台需要在onmessage里

    //console.log(event.data) //获取前端的obj对象，查看指针关系是否正确
    circuit=event.data.circuit  //这个js是执行功能电路EA生成的，因此要在circuit里面查找
    //目前传入的只有电源板，因此只暂时输出电源板相关信息
    _header=["产品或功能标志","功能","故障模式","故障原因","任务阶段与工作方式","局部影响","高一层影响","最终影响","故障率","故障模式频数比"]
    Header=generateHeader(_header) //生成表头
    //step1:生成电源板EA
    count=0     //统计最小的列表数
    col=new Array(_header.length)
  
    for(var i in circuit.children){
        _count1=0  //故障原因  eg:1.2.1.1a 共模电感L1 短路
        for(var j in circuit.children[i].children){
            //log(circuit.children[i].children[j])
            _count1+=circuit.children[i].children[j].children.length  //获取列：故障原因的个数
            for(var k in circuit.children[i].children[j].children){
                var objBad=circuit.children[i].children[j].children[k]

                if(typeof(col[3])=="undefined") col[3]=[]
                col[3].push(ci(
                    objBad.fatherItem.text+" "+objBad.text, //将其父对象元件名和其故障名称组合填入
                    1
                ))
            }
            if(typeof(col[2])=="undefined") col[2]=[]  //填入列：故障模式 eg:28V线性电源对地短路
            col[2].push(ci(
                circuit.children[i].children[j].text,
                circuit.children[i].children[j].children.length
            ))
            if(typeof(col[4])=="undefined") col[4]=[]  //填入列：任务阶段与工作方式 eg:全任务阶段
            col[4].push(ci(
               "全任务阶段",
                circuit.children[i].children[j].children.length
            ))

            if(typeof(col[5])=="undefined") col[5]=[]  //填入列：局部影响 eg:28V线性电源对地短路
            col[5].push(ci(
                circuit.children[i].children[j].text,
                circuit.children[i].children[j].children.length
            ))
            if(typeof(col[6])=="undefined") col[6]=[]  //填入列：高一层影响 eg:电源板28V线性电源故障
            col[6].push(ci(
                circuit.children[i].children[j].parent.text,
                circuit.children[i].children[j].children.length
            ))
            if(typeof(col[7])=="undefined") col[7]=[]  //填入列：最终影响 eg:氧气监控器控制功能丧失
            col[7].push(ci(
                circuit.children[i].children[j].parent.desc,
                circuit.children[i].children[j].children.length
            ))
            if(typeof(col[8])=="undefined") col[8]=[]   //故障率
            col[8].push(ci(
                circuit.children[i].lambda,
                circuit.children[i].children[j].children.length
            ))
            if(typeof(col[9])=="undefined") col[9]=[]   //故障模式频数比
            col[9].push(ci(
                circuit.children[i].children[j].lambda/circuit.children[i].lambda,
                circuit.children[i].children[j].children.length
            ))
        }
        if(typeof(col[1])=="undefined") col[1]=[]  //填入列：功能 eg:产生28V线性电源
        col[1].push(ci(
            circuit.children[i].desc,
            _count1
        ))
        if(typeof(col[0])=="undefined") col[0]=[]  //填入列：产品或功能标志 eg:28V线性电源电路
        col[0].push(ci(
            circuit.children[i].text,
            _count1
        ))
    }

    postMessage('<table id=\"data-table2\" border="1" cellspacing="0" width="100%" height="200">'+
    generateHtml(col,Header)+
    '</table>'
    )
}

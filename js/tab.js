tab={}
tab.generateHeader=function (list) { //产生表头,传入表头数组
    /**
     * <tr>
                <th>班级</t>
                <th>姓名</th>
                <th>年龄</th>
                <th>电话</th>
            </tr>
     */
    var str = "";
    str += "<tr>"
    for (var i in list) {
        str += "<th>" + list[i] + "</th>"
    }
    str += "</tr>"
    return str
}
tab.ci=function (_text,_span){
    return {
        text:_text,
        span:_span
    }
}

tab.generateHtml=function (col,header){//根据传送过来的col和header生成表格
    html=""
    //step1:生成表头
    html+=header
    //step2:生成行,一个tr表示一行，其中td表示行中的每一列，顺序排列
    count=0 //计算所有列
    for(var i in col[0]){
        count+=col[0][i].span
    }
    list=new Array(count) //新建那么多个的list项
    //step3：按列输出
    for(var i in col){
        var currentRow=0
        for(var j in col[i]){
            if(typeof(list[currentRow])=="undefined")
                list[currentRow]=[]
            list[currentRow].push(col[i][j])
            currentRow+=col[i][j].span //更新当前列
        }
    }
    //step4:生成表格
    for(var i in list){
        html+="<tr>"
        for(var j in list[i]){
            //<td rowspan="2">601班</td>
            html+="<td rowspan=\""+list[i][j].span+"\">"
            html+=list[i][j].text
            html+="</td>"
        }
        html+="</tr>"
    }

    return html
}

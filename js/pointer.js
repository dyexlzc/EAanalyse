/**
 * 用于在js中提供和C语言类似的指针功能 
 */
function randomString(len) {//生成随机字符串，用来定义变量名称
    len = len || 32;
    var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = $chars.length;
    var pwd = '';
    for (i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}
function newPoint() {
    vname = "_" + randomString()//默认生成32位变量名
    //log("var " + vname)
    if (typeof (eval("window." + vname)) == "undefined") {
        param = "{vname:\"" + vname + "\"}"
        eval("window." + vname + "=" + param)
        // console.log(eval(vname))
        return vname
    }
    else {
        return newPoint()
    }
}
function gp(vname) { //从指针中取值
    return eval(vname)
}
function addChild(parent, child) {
    /*
        作用，往parent中添加child
        此处传入的child是一个常量对象，需要将他转换成为pointer

    */
    if (typeof (parent.children) == "undefined") {
        parent.children = []
        var vname = eval(newPoint())  //申请指针
        vname.parent = parent //指针指回父对象
        // vname.p=child //将内容填充在指针的p属性中
        vname.text = child.text
        vname.desc = child.desc
        parent.children.push(vname) //填充进入子属性
    } else {
        var vname = eval(newPoint())  //申请指针
        vname.parent = parent //指针指回父对象
        vname.text = child.text
        vname.desc = child.desc
        parent.children.push(vname) //填充进入子属性
    }
    return vname
}
function addPChild(parent,p){
    //log(p)
    if(p.text=="无影响")
        return
    if (typeof (parent.children) == "undefined") {
        parent.children = []
        p.parent = parent //指针指回父对象
        parent.children.push(p) //填充进入子属性
    } else {
        p.parent = parent //指针指回父对象
        parent.children.push(p) //填充进入子属性
    }
}
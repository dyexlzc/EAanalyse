function generateHeader(list) { //产生表头,传入表头数组
    /**
     * <tr>
                <th>班级</t>
                <th>姓名</th>
                <th>年龄</th>
                <th>电话</th>
            </tr>
     */
    var str = "";
    str += "<tr>"
    for (var i in list) {
        str += "<th>" + list[i] + "</th>"
    }
    str += "</tr>"
    return str
}
function ci(_text,_span){
    return {
        text:_text,
        span:_span
    }
}
function generateHtml(col,header){//根据传送过来的col和header生成表格
    html=""
    //step1:生成表头
    html+=header
    //step2:生成行,一个tr表示一行，其中td表示行中的每一列，顺序排列
    count=0 //计算所有列
    for(var i in col[0]){
        count+=col[0][i].span
    }
    list=new Array(count) //新建那么多个的list项
    //step3：按列输出
    for(var i in col){
        var currentRow=0
        for(var j in col[i]){
            if(typeof(list[currentRow])=="undefined")
                list[currentRow]=[]
            list[currentRow].push(col[i][j])
            currentRow+=col[i][j].span //更新当前列
        }
    }
    //step4:生成表格
    for(var i in list){
        html+="<tr>"
        for(var j in list[i]){
            //<td rowspan="2">601班</td>
            html+="<td rowspan=\""+list[i][j].span+"\">"
            html+=list[i][j].text
            html+="</td>"
        }
        html+="</tr>"
    }

    return html
}
//*********************************************以上为function定义区 */


function log(v){
    console.log(v)
}
onmessage = function (event) { //前台传递给后台需要在onmessage里

    //console.log(event.data) //获取前端的obj对象，查看指针关系是否正确
    system=event.data
    //目前传入的只有电源板，因此只暂时输出电源板相关信息
    _header=["产品或功能标志","功能","故障模式","故障原因","任务阶段与工作方式","局部影响","高一层影响","最终影响"]
    Header=generateHeader(_header) //生成表头
    //step1:生成电源板EA
    count=0     //统计最小的列表数
    col=new Array(_header.length)
    for(var i in system.children){
        count+=system.children[i].children.length
        if(typeof(col[2])=="undefined")
            col[2]=[]
        col[2].push(ci(                     //列：故障模式
            system.children[i].text,
            system.children[i].children.length
        ))
        for(var j in system.children[i].children){  //列：故障原因
            if(typeof(col[3])=="undefined")
                col[3]=[]
            col[3].push(ci(
                system.children[i].children[j].text,
                1
            ))
        }
        if(typeof(col[4])=="undefined") col[4]=[]  //列：局部影响
        col[4].push(ci(
            "全任务阶段",
            system.children[i].children.length
        ))
        if(typeof(col[5])=="undefined") col[5]=[]  //列：局部影响
        col[5].push(ci(
            system.children[i].text,
            system.children[i].children.length
        ))
        if(typeof(col[6])=="undefined") col[6]=[]  //列：高一层影响
        col[6].push(ci(
            system.children[i].desc,
            system.children[i].children.length
        ))
        if(typeof(col[7])=="undefined") col[7]=[]  //列：最终影响
        col[7].push(ci(
            system.children[i].desc,
            system.children[i].children.length
        ))
    }

    col[0]=[
        ci("电源板",count)
    ]
    col[1]=[
        ci("产生产品运行所需电源",count)
    ]
    postMessage('<table border="1" cellspacing="0" width="100%" height="200">'+
    generateHtml(col,Header)+
    '</table>'
    )
}

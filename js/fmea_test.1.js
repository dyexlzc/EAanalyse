

function generateHeader(list) { //产生表头,传入表头数组
    /**
     * <tr>
				<th>班级</t>
				<th>姓名</th>
				<th>年龄</th>
				<th>电话</th>
			</tr>
     */
    var str="";
    str+="<tr>"
    for (var i in list) {
        str+="<th>"+list[i]+"</th>"
    }
    str+="</tr>"
    return str
}
function generateRowSpan(n,pstr){//产生n行合并的表格
    str=""
   // 电源板
    str+='<td rowspan="'+n+'">'
    str+=pstr
    str+="</td>"
    return str
}
function generateBadList(n,obj){//产生总体表格,采用手动建立dom树的方法,随后遍历生成HTML代码
    //console.log(obj)
    count=0
    for(var i in obj[n].bad){
        count+=obj[n].bad[i].children.length
    }
    //console.log(count)

    
    Header=generateHeader(["产品或功能标志","功能","故障模式","故障原因","任务阶段与工作方式","局部影响","高一层影响","最终影响"])
    treeDOM=[]  
    treeDOM[0]=({text:'<table border="1" cellspacing="0" width="100%" height="200">'})
    treeDOM[1]=({text:Header})//添加表头
    //step2.添加故障模式
    treeDOM[2]=({text:"tr",children:[]})//一个专门存放tr的数组,生成dom树的时候，自动在每个tr的前后增加<tr>标签
    //console.log(treeDOM)
    treeDOM[2].children.push({children:[]})


    treeDOM[2].children[0].children.push({text:'<th class="tg-0pky" rowspan="'+count+'">'+obj[n].text+'</th>'})
    treeDOM[2].children[0].children.push({text:'<th class="tg-0pky" rowspan="'+count+'">'+obj[n].tooltip+'</th>'})
    
      
    for (var i=0;i<count;i++){//添加对象
        //console.log(i)
        treeDOM[2].children.push({text:"a",children:[]})
    }
   // console.log("dom树",treeDOM)

   p=0
    for(var i in obj[n].bad){//添加故障模式
       //console.log(obj[n].bad[i])
        //treeDOM[2].children.push({text: "<th>"+obj[n].bad[i].text+"</th>" ,children:[]})
      //  treeDOM[2].children.splice(i*2,0,{children:[]})
        //console.log(treeDOM[2].children[i*2])
        treeDOM[2].children[p].children.push({text:'<th class="tg-0pky" rowspan="'+obj[n].bad[i].children.length+'">'+obj[n].bad[i].text+'</th>'})
        p+=obj[n].bad[i].children.length
    }

    p=0
    p2=0
    for (var i in obj[n].bad){//添加故障原因
        for(var j in obj[n].bad[i].children)
        {
            treeDOM[2].children[p].children.push({text:'<th class="tg-0pky" >'+ obj[n].bad[i].children[j].text+'</th>'})

            treeDOM[2].children[p].children.push({text:'<th class="tg-0pky" >全任务阶段</th>'})

            p++
        }
        //console.log(obj[n].bad[i].children.length)
        treeDOM[2].children[p2].children.push({text:'<th class="tg-0pky" rowspan="'+obj[n].bad[i].children.length+'">'+obj[n].bad[i].text+'</th>'})//局部影响
        treeDOM[2].children[p2].children.push({text:'<th class="tg-0pky" rowspan="'+obj[n].bad[i].children.length+'">'+obj[n].bad[i].bad_result+'</th>'})//高一层影响
        treeDOM[2].children[p2].children.push({text:'<th class="tg-0pky" rowspan="'+obj[n].bad[i].children.length+'">'+obj[n].bad[i].bad_result+'</th>'})//最终影响
        p2+=obj[n].bad[i].children.length

    }



    
    //treeDOM.push({tex4t:"</table>"})
    return treeDOM
}
function converTreeToHtml(obj){
    str=""
    console.log(obj)
    for(var i in obj){
        if(i<2){
            str+=obj[i].text 
        }
        else{
            //遍历th对象
            for(var j in obj[i].children)//每一个单独的tr
            {
                str+="<tr>"
                for(var k in obj[i].children[j].children){
                    item=obj[i].children[j].children[k]
                    str+=item.text
                }
                str+="</tr>"
            } 
        }
    }


    str+="</table>"
    return str
}

/**
 * 			<tr>
				<td rowspan="2">电源板</td>
				<td rowspan="2">产生产品运行所需电源</td>
				<td>Jack</td>
				<td>24</td>
				<td>1351234567</td>
			</tr>
 */


onmessage = function (event) { //前台传递给后台需要在onmessage里
   /* obj=JSON.parse(event.data)
    t=generateBadList(0,obj)//递归生成电源板EA:测试
    console.log(converTreeToHtml(t))
    postMessage(converTreeToHtml(t))*/
    console.log(event.data) //获取前端的obj对象，查看指针关系是否正确
    
    postMessage("aaa")
}

/**
 * 计算ca的单独js

 */

function calculation(name, param) {
    return Math.random();  //随机返回测试数据
}
function log(v) { console.log(v); }

onmessage = function (event) {
    //循环引用使用json-js来实现
    var system = event.data;  //先将对象保存
    //开始计算CA
    /**
     * CA计算的步骤
     * 第一步，计算所有元器件的λ
     * 第二部，往上迭代计算即可，calculation只在元器件级别起作用
     * 第三步，再次循环，计算频数比
     */
    for (var i in system._item) {
        var item = system._item[i];//获取每一个元器件的实例
        var total_lambda = 0.0;//元器件的每一个故障模式也有频数比
        for (var j in item.children) {
            //遍历其下的故障模式
            var bad = item.children[j];//获取故障模式的实例
            var lambda = calculation("电容", "text"); //计算出此错误的lambda
            bad['lambda'] = lambda
            total_lambda += lambda;
        }
        item["lambda"] = total_lambda;  //填入元器件的故障率
    }
    /**
     * 第二步，以电路维单位在故障模中循环，读取子对象的lambda且计算,功能电路
     * _badMood2是功能电路页面的故障模式
     */

    for (var i in system._circuit) { //这里是计算功能电路的CA的部分
        var total_lambda = 0.0;
        for (var k in system._circuit[i].children) {
            var bad = system._circuit[i].children[k];//获得故障模式的实例
            var lambda = 0.0;
            for (var j in bad.children) {
                //遍历每一个故障模式下面的元器件，读取其lambda
                var item = bad.children[j];//获取元器件的错误实例的附件
                lambda += item.lambda;//将孩子的lambda全部加上
                total_lambda += item.lambda; //计算总的故障率
            }
            bad['lambda'] = lambda;//将计算好的lambda填入
        }
        system._circuit[i]['lambda'] = total_lambda;//填入每一个功能电路的总
    }

    //这里是计算系统的CA，因为我们暂时以系统为最高层，因此不用循环所有系统
    var total_lambda = 0.0;
    for (var i in system._badMood) {
        var lambda = 0.0;
        for (var k in system._badMood[i].children) {
            total_lambda += system._badMood[i].children[k]["lambda"];
            lambda += system._badMood[i].children[k]["lambda"];
        }
        system._badMood[i]["lambda"] = lambda; //填入每一个故障的lambda，用于计算频数
    }
    system["lambda"] = total_lambda; //填入总的lambda，用于计算频数 




    /**
     * 第三步，计算频数比
     */
    for (var i in system._badMood) {
        system._badMood[i]["p"] = system._badMood[i]["lambda"] / system["lambda"];//计算频数
    }

    for (var i in system._circuit) {
        var circuit = system._circuit[i]; //获取每一个电路的实例
        for (var k in circuit.children) {
            var bad = circuit.children[k];//获取每一个故障模式的实例
            //每一个故障模式的频数比阿尔法为   自己的故障率/电路总的故障率
            bad["p"] = bad["lambda"] / circuit["lambda"];
        }
    }

    postMessage(system);
}

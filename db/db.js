
const Database = require('better-sqlite3');
const db = new Database('db/database.db', { verbose: console.log });

function findSValue(table, ss, t) {
    // 执行查询
    // let r = db.exec("SELECT sValue FROM " + table + " where t=" + t + " and s=" + ss + ";");
    //console.log("SELECT sValues FROM " + table + " where t=" + t + " and s=" + ss + ";");
    var row = db.prepare("SELECT sValues FROM "+table+" where t=? and s=?").get(t,ss);
    console.log(row);
    return row.sValues;
    /*db.get("SELECT sValue FROM cap_1 where t=20 and s = 0.1;", function (err, row) {
        if (err) throw err;
        console.log(row['sValue']);
       // return row;
    });*/
   /* let e = new Date().getTime();
    console.info("查询数据耗时：" + (e - s) + "ms");
    // 解析数据
    //let obj = dbToObj(r);
    console.info(r);
    return r[0].values[0][0];*/
}
function findSValue1(table, name) {
    // 执行查询
   /* let s = new Date().getTime();
    let r = db.exec("SELECT sValues FROM " + table + " where name='" + name + "';");
    let e = new Date().getTime();
    console.info("查询数据耗时：" + (e - s) + "ms");
    // 解析数据
    //let obj = dbToObj(r);
    console.info(r);
    return r[0].values[0][0];*/
    /*db.get("SELECT sValues FROM " + table + " where name='" + name + "';", function (err, row) {
        if (err) throw err;
        //console.log(row);
        return row['sValues'];
    });*/
    var row = db.prepare("SELECT sValues FROM "+table+" where name= ?").get(name);
    console.log(row);
    return row.sValues;
}
function cap_1_cv(param) {
    if (param["电容量"] < 7.5) { return  0.5 }
    else if (param["电容量"] < 91) { return 0.75 }
    else if (param["电容量"] < 470) { return 1.0 }
    else if (param["电容量"] < 2000) {return 1.3 }
    else if (param["电容量"] < 62000) {return 1.6 }
    else if (param["电容量"] < 160000) {return 1.9 }
    else if (param["电容量"] < 390000) { return 2.2 }
    else if (param["电容量"] > 390000) { return 2.4 }
}
function cap_2_cv() {
    if (param["电容量"] < 240) { value *= 0.5 }
    else if (param["电容量"] < 3300) { value *= 0.75 }
    else if (param["电容量"] < 16000) { value *= 1.0 }
    else if (param["电容量"] < 82000) { value *= 1.3 }
    else if (param["电容量"] < 270000) { value *= 1.6 }
    else if (param["电容量"] < 750000) { value *= 1.9 }
    else if (param["电容量"] < 1800000) { value *= 2.2 }
    else if (param["电容量"] > 1800000) { value *= 2.4 }
}
function cap_ta_cv() {
    if (param["电容量"] < 470000) { value *= 0.5 }
    else if (param["电容量"] < 3300000) { value *= 0.75 }
    else if (param["电容量"] < 15000000) { value *= 1.0 }
    else if (param["电容量"] < 47000000) { value *= 1.3 }
    else if (param["电容量"] < 100000000) { value *= 1.6 }
    else if (param["电容量"] < 220000000) { value *= 1.9 }
    else if (param["电容量"] < 500000000) { value *= 2.2 }
    else if (param["电容量"] > 500000000) { value *= 2.6 }
}
function cap_ta_sr() {
    if (param["串联电阻值"] < 0.2) { value *= 1.0 }
    else if (param["串联电阻值"] < 0.4) { value *= 0.8 }
    else if (param["串联电阻值"] < 0.6) { value *= 0.6 }
    else if (param["串联电阻值"] < 0.8) { value *= 0.4 }
    else if (param["串联电阻值"] < 1.0) { value *= 0.3 }
    else if (param["串联电阻值"] < 2.0) { value *= 0.2 }
    else if (param["串联电阻值"] < 3.0) { value *= 0.1 }
    else if (param["串联电阻值"] > 3.0) { value *= 0.07 }
}
function bjt_r() {
    if (param["额定功率"] < 0.7) { value *= 0.8 }
    else if (param["额定功率"] < 1) { value *= 1.0 }
    else if (param["额定功率"] < 5) { value *= 1.5 }
    else if (param["额定功率"] < 20) { value *= 2.2 }
    else if (param["额定功率"] < 50) { value *= 3.0 }
    else if (param["额定功率"] < 200) { value *= 4.0 }
    else if (param["额定功率"] > 200) { value *= 5.5 }
}
function jfet_r() {
    if (param["额定功率"] < 0.5) { value *= 0.8 }
    else if (param["额定功率"] < 1) { value *= 1.0 }
    else if (param["额定功率"] < 5) { value *= 1.5 }
    else if (param["额定功率"] < 20) { value *= 2.2 }
    else if (param["额定功率"] < 50) { value *= 3.0 }
    else if (param["额定功率"] < 200) { value *= 4.0 }
    else if (param["额定功率"] > 200) { value *= 5.5 }
}
function dio_s_r() {
    if (param["额定电流"] < 1) { value *= 1.0 }
    else if (param["额定电流"] < 3) { value *= 1.5 }
    else if (param["额定电流"] < 10) { value *= 2.0 }
    else if (param["额定电流"] < 20) { value *= 3.0 }
    else if (param["额定电流"] < 50) { value *= 4.0 }
    else if (param["额定电流"] > 50) { value *= 5.0 }
}
function pwp_r() {
    if (param["阻值"] < 20000) { value *= 1.0 }
    else if (param["阻值"] < 50000) { value *= 1.5 }
    else if (param["阻值"] < 100000) { value *= 2.0 }
    else if (param["阻值"] < 200000) { value *= 2.5 }
    else if (param["阻值"] < 500000) { value *= 3.5 }
    else if (param["阻值"] > 500000) { value *= 4.2 }
}
function smic_v() {
    v = findSValue(
        "smic_v",
        param["最恶劣环境状态结温"],
        param["实际工作电源电压"]
    )
}
function res_s_r() {
    if (param["阻值"] < 100000) { value *= 1 }
    else if (param["阻值"] < 1000000) { value *= 1.6 }
    else if (param["阻值"] > 1000000) { value *= 2.0 }
}
module.exports.calculate= function (type, param) {
    switch (type) {
        case "电容": {
            //console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "第一类瓷介电容器": {
                    
                    value *= findSValue(
                        "cap_1",
                        param["应力系数"],
                        param["温度"]
                    );
                    
                    value *= findSValue1("cap_1_e", param["环境系数"]);
                    value *= findSValue1("cap_1_q", param["质量系数"]);
                    value *= findSValue1("cap_1_ch", param["表贴系数"]);
                    value *= cap_1_cv(param);
                    
                    break;
                }
                case "第二类瓷介电容器": {
                    value *= findSValue(
                        "cap_2",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("cap_1_e", param["环境系数"]);
                    value *= findSValue1("cap_1_q", param["质量系数"]);
                    value *= findSValue1("cap_1_ch", param["表贴系数"]);
                    cap_2_cv;
                    break;
                }
                case "坦电容器": {
                    value *= findSValue(
                        "cap_ta",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("cap_1_e", param["环境系数"]);
                    value *= findSValue1("cap_1_q", param["质量系数"]);
                    value *= findSValue1("cap_1_ch", param["表贴系数"]);
                    cap_ta_cv;
                    cap_ta_sr;
                    break;
                }
            }
            console.log(value);
            return value;
        }
        case "电阻": {
            console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "片式电阻": {
                    value *= 0.007;       //基本失效率是个定值
                    value *= findSValue1("res_c_e", param["环境系数"]);
                    value *= findSValue1("res_c_q", param["质量系数"]);
                    res_c_r;
                    break;
                }
                    break;
            }
            return value;
            break;
        }
        case "半导体器件": {
            console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "双结型晶体管": {
                    value *= findSValue(
                        "bjt",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("bjt_e", param["环境系数"]);
                    value *= findSValue1("bjt_q", param["质量系数"]);
                    value *= findSValue1("bjt_a", param["应用系数"]);
                    value *= findSValue1("bjt_s", param["电应力系数"]);
                    value *= findSValue1("bjt_c", param["结构系数"]);
                    bjt_r;
                    break;
                }
                case "硅场效应晶体管": {
                    value *= findSValue(
                        "jfet",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("jfet_e", param["环境系数"]);
                    value *= findSValue1("jfet_q", param["质量系数"]);
                    value *= findSValue1("jfet_a", param["应用系数"]);
                    value *= findSValue1("jfet_s", param["电应力系数"]);
                    value *= findSValue1("jfet_c", param["结构系数"]);
                    jfet_r;
                    break;
                }
                case "普通二极管": {
                    value *= findSValue(
                        "dio_s",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("dio_s_e", param["环境系数"]);
                    value *= findSValue1("dio_s_q", param["质量系数"]);
                    value *= findSValue1("dio_s_a", param["应用系数"]);
                    value *= findSValue1("dio_s_s", param["电应力系数"]);
                    value *= findSValue1("dio_s_c", param["结构系数"]);
                    dio_s_r;
                    break;
                }
                case "稳压二极管": {
                    value *= findSValue(
                        "dio_z",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("dio_z_e", param["环境系数"]);
                    value *= findSValue1("dio_z_q", param["质量系数"]);
                    value *= 0.65
                    break;
                }
                case "稳流二极管": {
                    value *= findSValue(
                        "dio_z",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("dio_z_e", param["环境系数"]);
                    value *= findSValue1("dio_z_q", param["质量系数"]);
                    value *= 1.2
                    break;
                }
                    return value;
                    break;
            }
        }
        case "集成电路": {
            console.log(param);
            value = 1;
            c1 = 1; c2 = 1; c3 = 1; q = 1; wendu = 1; v = 1; l = 1; e = 1;
            switch (param["类别"]) {
                case "单片模拟电路": {
                    c1 = findSValue()
                    e = findSValue1("smic_e", param["环境系数"])
                    q = findSValue1("smic_q", param["质量系数"])
                    c1 = findSValue1("smic_c1", param["门数"])
                    c2 = findSValue1('smic_c2', param["门数"])
                    l = findSValue1("smic_l", param["成熟系数"])
                    wendu = findSValue1("smic_t", param["热点温度"])
                    switch (param["封装类型"]) {
                        case "密封": {
                            c3 = findSValue("smic_c3_s", param["门数"])
                            break
                        }
                        case "非密封": {
                            c3 = findSValue1("smic_c3_us", param["门数"])
                            break
                        }
                    }
                    if (param["实际工作电压"] < 8) { v = 1.0 }
                    else if (param["实际工作电压" < 12]) { v = 0.5 }
                    else if (param["实际工作电压"] < 18) { smic_v; }
                    value = q * (c1 * t * v + (c2 + c3) * e) * l;
                    return value;
                    break;
                }
                    return value;
                    break;
            }
        }
        case "电感器": {
            console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "片式电感": {
                    value *= 0.002
                    value *= findSValue1("ind_c_e", param["环境系数"]);
                    value *= findSValue1("ind_c_q", param["质量系数"]);
                    value *= findSValue1("ind_c_t", param["热点温度"]);

                    break;
                }
                    return value;
                    break;
            }
        }
        case "电位器": {
            console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "精密线绕电位器": {
                    value *= findSValue(
                        "pwp",
                        param["应力系数"],
                        param["温度"]
                    );
                    value *= findSValue1("pwp_e", param["环境系数"]);
                    value *= findSValue1("pwp_q", param["质量系数"]);
                    pwp_r();
                    break;
                }
                    return value;
                    break;
            }
        }
        case "振荡器与谐振器": {
            console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "振荡器": {
                    value *= 0.35;
                    value *= findSValue1("osc_e", param["环境系数"]);
                    value *= findSValue1("osc_q", param["质量系数"]);
                    break;
                }
                    return value;
                    break;
            }
        }
        case "其他": {
            console.log(param);
            value = 1;
            switch (param["类别"]) {
                case "电缆": {
                    value *= findSValue1("line", param["环境系数"]);
                    break;
                }
                    return value;
                    break;
            }
        }
    }

   /* console.log(
        calculate(
            "电容",
            {
                "电容量": 12,
                "类别": "第一类瓷介电容器",
                "温度": 25,
                "环境系数": 'Gb',
                "质量系数": 'A1p',
                "表贴系数": 'xian',
                "应力系数": 0.1,
                "串联电阻值": 0.3
            })
    );
    calculate(
        "电阻",
        {
            "电阻值": 12,
            "类别": "贴片电阻",
            "质量系数": 'Gb',
            "环境系数": 'A1p'
        })
    calculate(
        "集成电路",
        {
            "门数": 50,
            "类别": "单片模拟电路",
            "质量系数": 'A1',
            "环境系数": 'Gb',
            "成熟系数": "已成熟",
            "热点温度": 31,
            "封装类型": "密封",
            "封装方式": "DIP",
            "实际工作电源电压": 13,
            "最恶劣状态结温": 25
        })
    calculate(
        "半导体分立器件",
        {
            "电阻": 12,
            "类别": "双极型晶体管",
            "温度": 25,
            "应力系数": 0.1,
            "环境系数": 'Gb',
            "质量系数": 'A2',
            "应用系数": "高频",
            "结构系数": "单管",
            "电应力系数": 0.5,
            "种类系数": "结型",
            "额定功率": 40,
            "额定电流": 1
        })
    calculate(
        "电位器",
        {
            "温度": 25,
            "类别": "精密线绕电位器",
            "电应力系数": 0.5,
            "环境系数": 'Gb',
            "质量系数": 'A2',
            "电阻": 500,
        })
    calculate(
        "振荡器与谐振器",
        {
            "类别": "晶振",
            "环境系数": 'Gb',
            "质量系数": 'A2',
        })
    calculate(
        "电感",
        {

            "热点温度": 40,
            "类别": "片式电感",
            "环境系数": 'Gb',
            "质量系数": "A2"
        })
    calculate(
        "其他",
        {
            "环境系数": 'Gb',
            "类别": "导线"
        }
    );*/

}

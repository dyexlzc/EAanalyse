// Modules to control application life and create native browser window
const { app, BrowserWindow, dialog } = require('electron')

db = require("./db/db.js"); //引入数据库操作文件
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 800,
    webPreferences: {
      nodeIntegration: true
    }
  })
  //mainWindow.setMenu(null)//隐藏菜单栏
  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.打开自带的调试文件
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
  //下载相关
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

const ipc = require('electron').ipcMain;
ipc.on("ping", () => {
  var os = require('os'),
    iptable = {},
    ifaces = os.networkInterfaces();
  for (var dev in ifaces) {
    ifaces[dev].forEach(function (details, alias) {
      if (details.family == 'IPv4') {
        iptable[dev + (alias ? ':' + alias : '')] = details.address;
      }
    });
  }
  console.log(iptable);
  // mainWindow.webContents.on("callback", () => {
  mainWindow.webContents.send("callback", iptable);
  //})

})


//数据库操作相关
ipc.on("db", (event, param) => {
  //接受从前台传来的数据查询操作,param中为已经stringly的对象
  console.log(db.calculate(
    "电容",
    {
        "电容量": 12,
        "类别": "第一类瓷介电容器",
        "温度": 25,
        "环境系数": 'Gb',
        "质量系数": 'A1p',
        "表贴系数": 'xian',
        "应力系数": 0.1,
        "串联电阻值": 0.3
    })
  );
  mainWindow.webContents.send("callback", db.calculate(
    "电容",
    {
        "电容量": 12,
        "类别": "第一类瓷介电容器",
        "温度": 25,
        "环境系数": 'Gb',
        "质量系数": 'A1p',
        "表贴系数": 'xian',
        "应力系数": 0.1,
        "串联电阻值": 0.3
    }));
  
})
